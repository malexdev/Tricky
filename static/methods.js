Tricky.call = function() {
    // convert arguments to a real array
    var args = [].slice.apply(arguments);

    // ensure that we have at least one argument (the function to call on the server)
    if (args.length === 0) { return console.log('No arguments supplied to Tricky.call'); }

    // get the target function
    var target = args.shift();

    // get the callback, if we were passed one
    var callback;
    if (typeof args[args.length - 1] === 'function') { callback = args.pop(); }

    // send the args to the target on the server
    var payload = {target: target, args: args};
    Tricky.socket.emit('method', payload, callback);
};
