// set up tricky data
window.Tricky = {
    socket: {},
    vars: {
        baseUrl: '/tricky',
        socketid: ''
    },
    script: {
        load: function(path, callback) {
            var script = document.createElement('script');
            script.setAttribute('type', 'text/javascript');
            script.setAttribute('src', path);
            script.onload = callback;
            document.getElementsByTagName('body')[0].appendChild(script);
        }
    },
    ready: function() {
        console.log('Tricky initialized with socket ID [%s]', Tricky.vars.socketid);
    }
};

// load socket io
Tricky.script.load('/socket.io/socket.io.js', function() {
    Tricky.socket = io('http://' + location.host + '/tricky/socket');

    // listen to server startup message
    Tricky.socket.on('startup', function(socketid) {
        Tricky.vars.socketid = socketid;

        // run Tricky ready function
        Tricky.ready();
    });
});

// load Tricky libs
Tricky.script.load('/tricky/static/methods.js');
