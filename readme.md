# Tricky

Tricky was heavily inspired by [Meteor](https://www.meteor.com/).
One of the primary goals of Tricky was to bring Meteor-inspired functionality to existing Node projects, or for projects where using Meteor doesn't make sense.

Do note that while Meteor is open source, Tricky does not actually use any of Meteor's code to do things: this was a decision made because standard Node and Meteor work quite differently.
Additionally, while the original design intent of Tricky was based upon Meteor, it will not always be so. It does not follow Meteor's spec, and may implement features that Meteor doesn't have.

### Build Status [![Build Status](https://travis-ci.org/codechimera/Tricky.svg?branch=master)](https://travis-ci.org/codechimera/Tricky)

Tested against every new `major.minor` build of node from 4.0 and up. For details, check `.travis.yml`.

### Installation

- Use `npm install tricky` to use the latest published version in your project

### Submitting Issues

 - Open an issue with the test(s) that prove the issue.
 - Preferably, submit a pull request with the test(s) and the fix!

### Submitting new features

 - Open an issue with the feature idea.
 - Preferably, submit tests for the feature.
 - Even better, submit a pull request with the tests and the feature implemented!

### Setting up Tricky

- In your server, give Tricky a variable: `var tricky = require('tricky');`
- Set up Tricky on your web server with `tricky.init(server);`. This takes an HTTP server. For use with Express, see below.
- Set up Tricky on your client with `<script type="application/javascript" src="/tricky-client.js"></script>`.
    - This needs to be before any code that depends on Tricky, and like all JavaScript it should be at the end of your `<body>`.
    - Tricky will run `Tricky.ready()` when it's finished starting up, so if you want to do things afterwards just override that function.
    - Tricky registers a global variable, `Tricky`, in your browser.

### Tricky with Express

Express provides an HTTP server object, so use it something like the following:

````javascript
var Tricky = require('tricky');

// set up express
var express = require('express');
var app = express();
var server = require('http').createServer(app);

// set up tricky
Tricky.init(server);
````

Note that the [Tricky Demo](https://github.com/codechimera/Tricky-Demo) uses Express, so it offers a real-world use case of Tricky to look at.

# Using Tricky

### Tricky Methods

A clone of [Meteor methods](http://docs.meteor.com/#/basic/methods), Tricky methods allow you to call a function from the client that runs on the server, then sends back the response.

- On the server, construct a method with a given name and with the parameters you expect to have passed to the method. The callback is optional, but if you want to send data back to the client it's required.
- On the client, call the method using its name and pass any parameters to the method. These parameters are passed wholesale to the server to be executed, and the response is sent directly back to the client.

##### Server

````javascript
Tricky.methods({
    add5: function(input, callback) {
        callback(null, input + 5);
    }
});
````

##### Client

````javascript
Tricky.call('add5', 10, function(err, response) {
    console.log('Response: %d', response);
});
````
