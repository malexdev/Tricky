// require things
var debug = require('debug')('tricky:main');
var path = require('path');

// set up Tricky
var tricky = {
    session: require(path.join(__dirname, 'lib', 'session')),
    init: initServer,
    lib: {
        methods: require(path.join(__dirname, 'lib', 'methods')),
        server: require(path.join(__dirname, 'lib', 'server')),
        static: require(path.join(__dirname, 'lib', 'static'))
    },
    listeners: {
        methods: require(path.join(__dirname, 'listeners', 'methods'))
    },
    methods: require(path.join(__dirname, 'lib', 'methods')).set
};

// init the package
module.exports = tricky;

// initialize Tricky on the provided server object
function initServer(server) {
    debug('Initializing Tricky');

    // set up socket.io
    var io = require('socket.io')(server);

    // create a namespace for tricky
    tricky.io = io.of('/tricky/socket');

    // set up a listener for when clients connect
    tricky.io.on('connection', function(socket) {
        debug('Tricky client connected with ID [%s]', socket.id);

        // set up listeners
        tricky.listeners.methods.init(socket);

        // tell the client we're ready to go
        tricky.io.emit('startup', socket.id);
    });

    // serve client js file
    tricky.lib.server.attach(server);
}
