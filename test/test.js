const should = require('should');
const decache = require('decache');
const path = require('path');

global.UNITTEST = true;
global.suites = path.join(__dirname, 'suites');
global.root = path.join(__dirname, '..');

const trickyPath = path.join(global.root, 'tricky');
global.tricky = require(trickyPath);

describe('Tricky', function() {

    beforeEach(function() {
        decache(trickyPath);
        global.tricky = require(trickyPath);
    });

    describe('main', function() {
        require(path.join(suites, 'tricky'));
    });

    describe('listeners', function() {
        require(path.join(suites, 'listeners', 'methods'));
    });

    describe('lib', function() {
        require(path.join(suites, 'lib', 'static'));
        require(path.join(suites, 'lib', 'session'));
    });
});
