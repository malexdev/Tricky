const path = require('path');
const should = require('should');
const fs = require('fs');

describe('static.js', function() {

    describe('exports', function() {
        it('should export an object with the contents of the files in the static directory', function() {
            const dirpath = path.join(global.root, 'static');
            const files = fs.readdirSync(dirpath);
            const contents = {};

            // load the files from the directory into the contents hash
            files.forEach(file => contents[file] = fs.readFileSync(path.join(dirpath, file), 'utf8'));
            const staticDir = require(path.join(root, 'lib', 'static'));

            should.exist(staticDir);
            should.deepEqual(staticDir, contents);
        });
    });

});
