const path = require('path');
const should = require('should');

const testKey = 'test', testVal = 1;

describe('static.js', function() {

    describe('exports', function() {
        it('should export {get, set}', function() {
            const session = require(path.join(global.root, 'lib', 'session'));
            should.exist(session);
            session.should.have.keys(['get', 'set', '_internal']);
        });
    });

    describe('get()', function() {
        it('should return a value that was set', function() {
            const session = require(path.join(global.root, 'lib', 'session'));

            session._internal[testKey] = testVal;
            const result = session.get(testKey);

            should.exist(result);
            result.should.equal(testVal);
        });

        it('should return null for a value that was not set', function() {
            const session = require(path.join(global.root, 'lib', 'session'));
            delete session._internal[testKey];

            const result = session.get(testKey);
            should.not.exist(result);
        });
    });

    describe('set()', function() {
        it('should set a value that can be retrieved later', function() {
            const session = require(path.join(global.root, 'lib', 'session'));
            session.set(testKey, testVal);

            should.exist(session._internal[testKey]);
            session._internal[testKey].should.equal(testVal);
        });
    });

});
