const should = require('should');
const http = require('http');
const request = require('request');
const port = 3333;

describe('tricky.js', function() {

    describe('exports', function() {

        it('should export: {session, init, lib: {methods, server, static}, listeners: {methods}, methods}', function() {
            should.exist(tricky);
            tricky.should.have.keys(['session', 'init', 'lib', 'listeners', 'methods']);
            tricky.lib.should.have.keys(['methods', 'server', 'static']);
            tricky.listeners.should.have.key('methods');
            tricky.originalCache = true;
        });

        it('should have reloaded Tricky from cache before the next test', function() {
            should.exist(tricky);
            should.not.exist(tricky.originalCache);
        });

    });

    describe('initServer()', function() {

        it('should bind to an HTTP server and serve the tricky client', function(done) {
            const app = http.createServer(function(req, res) {
                res.writeHead(200);
                res.end('Tricky Test');
            });

            tricky.init(app);

            app.on('listening', function() {
                request('http://localhost:3333/tricky-client.js', function(err, response, body) {
                    should.not.exist(err);

                    should.exist(body);

                    should.exist(response.statusCode);
                    response.statusCode.should.equal(200);

                    app.close();
                    done();
                });
            });

            app.listen(port);
        });

        it('should allow non-tricky routes to function on the server', function(done) {
            const testString = 'Tricky Test';

            const app = http.createServer(function(req, res) {
                res.writeHead(200);
                res.end(testString);
            });

            tricky.init(app);

            app.on('listening', function() {
                request('http://localhost:3333', function(err, response, body) {
                    should.not.exist(err);

                    should.exist(body);
                    body.should.equal(testString);

                    should.exist(response.statusCode);
                    response.statusCode.should.equal(200);

                    app.close();
                    done();
                });
            });

            app.listen(port);
        })

    })

});
