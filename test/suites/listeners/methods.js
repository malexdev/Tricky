const path = require('path');
const should = require('should');

describe('methods.js', function() {

    describe('exports', function() {
        it('should export init', function() {
            const methods = require(path.join(global.root, 'listeners', 'methods'));
            should.exist(methods);
            methods.should.have.keys('init');
        });
    });

    describe('init()', function() {
        it('should add the method handler');
    });

    describe('handleTrickyMethod()', function() {
        it('should run the passed method');
        it('should return an error if passed no method name to run');
        it('should return an error if passed an invalid method name');
    });
});
