var path = require('path');
var methods = require(path.join(__dirname, '..', 'lib', 'methods'));

// initialize Tricky method handling on the socket
function init(socket) {
    socket.on('method', handleTrickyMethod);
}

// handle a given Tricky method
function handleTrickyMethod(payload, callback) {
    // check if there's a registered method by the name requested
    if (methods.exists(payload.target)) {
        methods.run(payload.target, payload.args, callback);
    } else {
        callback('Method does not exist');
    }
}

module.exports = {
    init
};
