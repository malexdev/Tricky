var debug = require('debug')('tricky:lib:static');
var path = require('path');
var readdir = require('read-dir');

// load the source for all the static files
var staticPath = path.join(__dirname, '..', 'static');
var staticDir = readdir(staticPath);

// load each file in the folder
debug('Read contents of [%s] to memory for static serving', staticPath);
debug('File list: %j', Object.keys(staticDir));

module.exports = staticDir;
