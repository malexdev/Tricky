var debug = require('debug')('tricky:lib:server');
var path = require('path');
var staticDir = require(path.join(__dirname, 'static'));

var trickyBaseUrl = '/tricky/';
var trickyStaticUrl = trickyBaseUrl + 'static/'
var trickyClientUrl = '/tricky-client.js';

function attachClient(server) {
    // get a list of other handlers
    var externalHandlers = server.listeners('request').slice(0);

    // remove other handlers from the request server
    server.removeAllListeners('request');

    // when we get a request
    server.on('request', function(req, res) {
        debug('Server request: %s', req.url);

        // check if the request is for our client code
        if (req.url.indexOf(trickyClientUrl) === 0) {
            serveStatic('client.js', req, res);
        } else if (req.url.indexOf(trickyStaticUrl) === 0) {
            // serve the tricky static resource
            var trickyResource = req.url.replace(trickyStaticUrl, '');
            serveStatic(trickyResource, req, res);
        } else {
            // since the request wasn't our client code, pass on the request to all the handers we noticed earlier
            for (var i = 0; i < externalHandlers.length; i++) {
                externalHandlers[i].call(server, req, res);
            }
        }
    });

    debug('Attached Tricky client server');
}

function serveStatic(url, req, res) {
    debug('Serving Tricky static resource at [%s]', url);
    if (staticDir[url]) {
        res.setHeader('Content-Type', 'application/javascript');
        res.writeHead(200);
        res.end(staticDir[url]);
    } else {
        res.writeHead(404);
        res.end('No Tricky resource exists for the URL specified');
    }
}

module.exports.attach = attachClient;
