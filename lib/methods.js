var debug = require('debug')('tricky:lib:methods');

// store Tricky methods
var methodStorage = {};

function add(name, method) {
    methodStorage[name] = method;
}

function set(methods) {
    var keys = Object.keys(methods);
    keys.forEach(function(key) {
        add(key, methods[key]);
    });
}

function run(name, args, callback) {
    if (methodStorage[name]) {
        debug('Running method [%s] with args %j %s', name, args, callback ? 'and callback' : '');

        // set the callback to the last argument
        args.push(callback);

        // run the method
        methodStorage[name].apply(this, args);
    }
}

function exists(name) {
    return methodStorage[name] ? true : false;
}

function remove(name) {
    if (methodStorage[name]) {
        delete methodStorage[name];
    }
}

// set up method storage
module.exports = {
    add,
    set: set,
    run,
    exists,
    remove
}
