const session = {};

function get(key) {
    return session[key];
}

function set(key, value) {
    session[key] = value;
    return session;
}

module.exports = {
    get: get,
    set: set
};

if (global.UNITTEST) {
    module.exports._internal = session;
}
